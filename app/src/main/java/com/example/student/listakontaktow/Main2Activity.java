package com.example.student.listakontaktow;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.student.listakontaktow.R;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    // "Send text back" button click
    public void addItem(View view) {

        // get the text from the EditText
        EditText imie = (EditText) findViewById(R.id.imieEdit);
        EditText nazwisko = (EditText) findViewById(R.id.nazwiskoEdit);
        EditText telefon = (EditText) findViewById(R.id.telefonEdit);
        String stringToPassBack1 = imie.getText().toString();
        String stringToPassBack2 = nazwisko.getText().toString();
        String stringToPassBack3 = telefon .getText().toString();

        // put the String to pass back into an Intent and close this activity
        Intent intent = new Intent();
        intent.putExtra("keyName1", stringToPassBack1);
        intent.putExtra("keyName2", stringToPassBack2);
        intent.putExtra("keyName3", stringToPassBack3);
        setResult(RESULT_OK, intent);
        finish();
    }
}
