package com.example.student.listakontaktow;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;

import java.util.List;

public class MainActivity extends ListActivity {
    ZarzadcaBazy zb = new ZarzadcaBazy(this);
    private static final int SECOND_ACTIVITY_RESULT_CODE = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        /*zb.dodajKontakt(new Kontakt("Jan","Kowalski", "22 505 555 555"));
        zb.dodajKontakt(new Kontakt("Jan","Nowak", "42 545 666 554"));
        zb.dodajKontakt(new Kontakt("Tomasz","Iksinski", "112"));
        for(Kontakt k:zb.dajWszystkie()) {
            Log.d("DB", k.getNr()+" "+k.getImie()+" "+k.getNazwisko()+
                    " "+k.getTelefon());
        }*/

        /*Kontakt toChange = new Kontakt("Tomasz", "Igrekowski", "112");
        toChange.setNr(6L);
        zb.aktualizujKontakt(toChange);
        for(Kontakt k:zb.dajWszystkie()) {
            Log.d("DB", k.toString());
        }*/

        /*for(Kontakt k:zb.dajWszystkie()) {
            zb.kasujKontakt(k.getNr().intValue());
        }*/
        updateListView();
    }

    private void updateListView() {
        List<Kontakt> values = zb.dajWszystkie();
        ArrayAdapter<Kontakt> adapter = new ArrayAdapter<Kontakt>(this,
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
    }

    public void deleteAll(View view){
        for(Kontakt k:zb.dajWszystkie()) {
            zb.kasujKontakt(k.getNr().intValue());
           //setContentView(R.layout.activity_main);
            updateListView();
        }
    }
    public void addNewItem(View view){
    Intent intent = new Intent(this, Main2Activity.class);
    startActivityForResult(intent, SECOND_ACTIVITY_RESULT_CODE);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // check that it is the SecondActivity with an OK result
        if (requestCode == SECOND_ACTIVITY_RESULT_CODE) {
            if (resultCode == RESULT_OK) {

                // get String data from Intent
                String returnString1 = data.getStringExtra("keyName1");
                String returnString2 = data.getStringExtra("keyName2");
                String returnString3 = data.getStringExtra("keyName3");
                //count = Integer.parseInt(returnString);
                // set text view with string
                //TextView textView = (TextView) findViewById(R.id.counter);
                //textView.setText(returnString);
                zb.dodajKontakt(new Kontakt(returnString1,returnString2, returnString3));
                updateListView();
            }
        }
    }
}

