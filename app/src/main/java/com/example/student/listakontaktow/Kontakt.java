package com.example.student.listakontaktow;

/**
 * Created by student on 12/11/16.
 */

public class Kontakt {

    private Long nr;
    private String imie;
    private String nazwisko;
    private String telefon;

    public Kontakt(){

    }
    public Kontakt(String imie, String nazwisko, String telefon){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.telefon = telefon;
    }
    public void setNr(Long nr) {
        this.nr = nr;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public Long getNr() {

        return nr;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getImie() {
        return imie;
    }

    public String getTelefon() {
        return telefon;
    }
    public String toString() {
        return imie + " " + nazwisko + " " + telefon;
    }
}
